# frozen_string_literal: true

require 'sinatra'

# Simple api for testing purposes
class Api < Sinatra::Base
  get '/' do
    'Hello World!'
  end
end
