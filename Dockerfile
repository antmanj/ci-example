FROM ruby:3.1.2-alpine3.15 as base

ENV BUNDLE_PATH '/gems'
ENV BUNDLE_DEPLOYMENT 'true'
ENV BUNDLE_WITHOUT 'development'

RUN gem update --system '3.3.14' -N
RUN gem install bundler -v '~>2.3.14' -N

WORKDIR /app

FROM base as build

RUN apk --no-cache add make gcc libc-dev g++ gcc build-base icu-dev git

COPY Gemfile Gemfile.lock /app/
RUN bundle install --jobs 4 --retry 3

FROM base

COPY --from=build /gems /gems
COPY --from=build /app /app
COPY lib ./lib
COPY config.ru ./

CMD [ "bundle", "exec", "puma" ]