## Repository to illustrate a simple CI/CD for a dockerized Ruby application


## Run locally
```
bundle install
bundle exec puma
```

## Run tests locally
```
bundle install
bundle exec rspec
```